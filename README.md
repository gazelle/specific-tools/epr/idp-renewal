# CH IDP Renewal

This project is a gateway to add support to WS-trust renewal operation over Shibboleth CH IDP. It
requires CH IDP to be deployed.

## SOAPUI Project

The SOAP UI can be found here : <https://gitlab.inria.fr/gazelle/specific-tools/epr/no-peer-testing/-/tree/feature/IDP_renewal/IDP>

## Installation documentation

The installation documentation could be found at : <https://gitlab.inria.fr/gazelle/documentation/gazelle-user-documentation/-/blob/master/Authentication-Simulator/installation.md>