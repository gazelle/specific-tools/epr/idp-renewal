package net.ihe.gazelle.renewal;

import net.ihe.gazelle.chidp.renewal.application.IDPAuthnClient;
import org.opensaml.saml2.core.Assertion;

public class IDPAuthClientMock implements IDPAuthnClient {

    public IDPAuthClientMock() {
    }

    @Override
    public Assertion authenticate(String issuer, String username, String password) {
        return null;
    }
}
