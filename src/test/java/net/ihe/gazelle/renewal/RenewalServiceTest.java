package net.ihe.gazelle.renewal;

import net.ihe.gazelle.chidp.renewal.application.ExpiredAssertionException;
import net.ihe.gazelle.chidp.renewal.application.RenewalService;
import net.ihe.gazelle.chidp.renewal.application.RenewalServiceImpl;
import org.joda.time.DateTime;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import org.opensaml.saml2.core.Assertion;
import org.opensaml.xml.io.UnmarshallingException;

import javax.xml.bind.JAXBException;
import java.io.FileNotFoundException;

import static org.junit.jupiter.api.Assertions.fail;

class RenewalServiceTest {

    IDPAuthClientMock idpAuthClientMock = new IDPAuthClientMock();
    RenewalConfigMock renewalConfigMock = new RenewalConfigMock();

    RenewalService renewalService = new RenewalServiceImpl(idpAuthClientMock, renewalConfigMock);

    RenewalServiceTest() {
    }

    @Test
    void testNullAssertion() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> renewalService.renew(null));
    }

    @Test
    @Disabled("Feature is not yet implemented")
    void testInvalidAssertion() {
        fail("To implement");
    }

    @Test
    @Disabled("Feature is not yet implemented")
    void testCorruptedAssertion() {

        fail("To implement");
    }

    @Test
    void testExpiredRenewalAssertion() throws JAXBException, FileNotFoundException, UnmarshallingException {
        Assertion assertion = new RenewalIDPBuilder().getAssertionFromXml("idp-renewal-request.xml");
        int minutesToAdd = new RenewalConfigMock().getRenewalPeriodMinutes() + 2;
        assertion.getConditions().setNotOnOrAfter(new DateTime().plusMinutes(minutesToAdd));
        Assertions.assertThrows(ExpiredAssertionException.class, () -> renewalService.renew(assertion));
    }

    @Test
    void testRenewAssertion() throws JAXBException, FileNotFoundException, UnmarshallingException {
        Assertion assertion = new RenewalIDPBuilder().getAssertionFromXml("idp-renewal-request.xml");
        assertion.getConditions().setNotOnOrAfter(new DateTime());
        Assertions.assertDoesNotThrow(() -> renewalService.renew(assertion));
    }

}
