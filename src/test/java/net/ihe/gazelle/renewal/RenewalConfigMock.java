package net.ihe.gazelle.renewal;

import net.ihe.gazelle.chidp.renewal.interlay.ws.RenewalConfig;

public class RenewalConfigMock implements RenewalConfig {
    @Override
    public String getIdpEcpUrl() {
        return null;
    }

    @Override
    public String getUserTestPassword() {
        return null;
    }

    @Override
    public int getRenewalPeriodMinutes() {
        return 120;
    }
}
