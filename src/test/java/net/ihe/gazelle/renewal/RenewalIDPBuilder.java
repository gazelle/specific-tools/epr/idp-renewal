package net.ihe.gazelle.renewal;


import net.ihe.gazelle.chidp.renewal.application.RenewalService;
import net.ihe.gazelle.chidp.renewal.interlay.ws.ChIdpRenewalWSImpl;
import net.ihe.gazelle.chidp.renewal.interlay.ws.SAML2AssertionMarshaller;
import org.oasis_open.docs.ws_sx.ws_trust._200512.*;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.xml.io.UnmarshallingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Element;

import javax.inject.Inject;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;
import javax.xml.namespace.QName;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.text.MessageFormat;


public class RenewalIDPBuilder {

    private static final Logger LOG = LoggerFactory.getLogger(ChIdpRenewalWSImpl.class);

    static final String SOAP_12_ENV_NS = "http://www.w3.org/2003/05/soap-envelope";
    static final QName SENDER_ENV_FAULT_CODE = new QName(SOAP_12_ENV_NS, "Sender");
    static final QName RECEIVER_ENV_FAULT_CODE = new QName(SOAP_12_ENV_NS, "Receiver");

    static final String WSTRUST_13_NS = "http://docs.oasis-open.org/ws-sx/ws-trust/200512";
    static final String RENEW_REQUEST_TYPE = WSTRUST_13_NS + "/Renew";
    static final QName REQUEST_TYPE_WSTRUST_ELEMENT = new QName(WSTRUST_13_NS, "RequestType");
    static final QName TOKEN_TYPE_WSTRUST_ELEMENT = new QName(WSTRUST_13_NS, "TokenType");
    static final QName INVALID_REQUEST_WSTRUST_FAULT_SUB_CODE = new QName(WSTRUST_13_NS, "InvalidRequest");
    static final String SAML2_TOKEN_PROFILE_TYPE =
            "http://docs.oasis-open.org/wss/oasis-wss-saml-token-profile-1.1#SAMLV2.0";
    public static final QName RENEW_TARGET_WSTRUST_ELEMENT = new QName(WSTRUST_13_NS, "RenewTarget");
    private static final ObjectFactory WS_TRUST_FACTORY = new ObjectFactory();
    private static final org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_utility_1_0.ObjectFactory UTIL_FACTORY =
            new org.oasis_open.docs.wss._2004._01.oasis_200401_wss_wssecurity_utility_1_0.ObjectFactory();


    @Inject
    private RenewalService renewalService;
    SAML2AssertionMarshaller assertionMarshaller = new SAML2AssertionMarshaller();


    public RenewalIDPBuilder() {
    }

    public Assertion getAssertionFromXml(String nameOfFile) throws JAXBException, FileNotFoundException, UnmarshallingException {
        RequestSecurityTokenType req = getRequestFromXmlFile(nameOfFile);
        return assertionMarshaller.unmarshall(getAssertionElement(req));
    }

    public RequestSecurityTokenType getRequestFromXmlFile(String file) throws JAXBException, FileNotFoundException {
        // Create a JAXBContext object
        JAXBContext jaxbContext = JAXBContext.newInstance(RequestSecurityTokenType.class);
        // Create an Unmarshaller object
        Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        // Unmarshall the XML file
        JAXBElement<RequestSecurityTokenType> request = (JAXBElement<RequestSecurityTokenType>) unmarshaller.unmarshal(new FileInputStream("src/test/resources/" + file));
        return request.getValue();

    }

    private Element getAssertionElement(RequestSecurityTokenType request) {
        RenewTargetType renewTarget = getElementValue(request, RENEW_TARGET_WSTRUST_ELEMENT, RenewTargetType.class);
        return renewTarget != null ? getAssertionElement(renewTarget) : null;
    }

    private Element getAssertionElement(RenewTargetType renewTarget) {
        if (renewTarget.getAny() != null) {
            if (Element.class.isAssignableFrom(renewTarget.getAny().getClass())) {
                Element renewTargetValueElement = (Element) renewTarget.getAny();
                if (RenewalService.SAML_2_0_ASSERTION_NS.equals(renewTargetValueElement.getNamespaceURI())
                        && RenewalService.ASSERTION_TAGNAME.equals(renewTargetValueElement.getLocalName())) {
                    return renewTargetValueElement;
                }
            } else {
                throw new ClassCastException(
                        MessageFormat.format("Value of element {0} cannot be cast as {1}.", RENEW_TARGET_WSTRUST_ELEMENT,
                                Element.class));
            }
        }
        return null;
    }

    private <T> T getElementValue(RequestSecurityTokenType request,
                                  QName elementName,
                                  Class<T> valueClass) {
        for (Object any : request.getAny()) {
            if (elementName.equals(((JAXBElement<?>) any).getName())) {
                if (((JAXBElement<?>) any).getDeclaredType().isAssignableFrom(valueClass)) {
                    return ((JAXBElement<T>) any).getValue();
                } else {
                    throw new ClassCastException(
                            MessageFormat.format("Value of element {0} cannot be cast as {1}.", elementName, valueClass));
                }
            }
        }
        return null;
    }

}
