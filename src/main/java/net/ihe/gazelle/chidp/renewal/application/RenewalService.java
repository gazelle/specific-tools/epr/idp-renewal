package net.ihe.gazelle.chidp.renewal.application;


import org.opensaml.saml2.core.Assertion;

/**
 * SAML 2 Assertion Renewal service in the context of CH-IDP.
 */
public interface RenewalService {

   String SAML_2_0_ASSERTION_NS = "urn:oasis:names:tc:SAML:2.0:assertion";
   String ASSERTION_TAGNAME = "Assertion";
   String ISSUER_TAGNAME = "Issuer";
   String NAME_ID_TAGNAME = "NameID";

   /**
    * Request for an assertion renewal.
    *
    * @param assertion to renew
    *
    * @return the renewed assertion.
    *
    * @throws IllegalArgumentException  in case of missing or bad assertion
    * @throws ExpiredAssertionException if the assertion to renew has outranged the renewal expiration period.
    * @throws AuthenticationException   in case the back-end idp has refused to renew the session.
    */
   Assertion renew(Assertion assertion);

}
