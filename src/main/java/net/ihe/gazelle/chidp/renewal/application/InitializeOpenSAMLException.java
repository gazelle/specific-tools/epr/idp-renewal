package net.ihe.gazelle.chidp.renewal.application;

public class InitializeOpenSAMLException extends RuntimeException{

    public InitializeOpenSAMLException() {
    }

    public InitializeOpenSAMLException(String message) {
        super(message);
    }

    public InitializeOpenSAMLException(String message, Throwable cause) {
        super(message, cause);
    }

    public InitializeOpenSAMLException(Throwable cause) {
        super(cause);
    }
}
