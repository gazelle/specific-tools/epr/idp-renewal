package net.ihe.gazelle.chidp.renewal.application;

import net.ihe.gazelle.chidp.renewal.interlay.ws.RenewalConfig;
import org.opensaml.saml2.core.Assertion;

import java.util.Date;
import java.util.concurrent.TimeUnit;

public class RenewalServiceImpl implements RenewalService {

   private final IDPAuthnClient authnClient;
   private final RenewalConfig renewalConfig;

   public RenewalServiceImpl(IDPAuthnClient idpAuthnClient, RenewalConfig renewalConfig) {
      this.authnClient = idpAuthnClient;
      this.renewalConfig = renewalConfig;
   }

   @Override
   public Assertion renew(Assertion assertion) {
      if (assertion == null) {
         throw new IllegalArgumentException("Assertion must be defined or not null");
      }

      // TODO verify Assertion is valid regarding CH schema, throw IllegalArgumentException if not.
      // TODO verify Assertion is not corrupted (signature valid), throw IllegalArgumentException if not.

      checkValidityTime(assertion);

      return authnClient.authenticate(
            getAudience(assertion),
            getUsername(assertion),
            renewalConfig.getUserTestPassword()
      );
   }

   private String getUsername(Assertion assertion) {
      return assertion.getSubject().getNameID().getValue();
   }

   private String getAudience(Assertion assertion) {
      return assertion.getConditions().getAudienceRestrictions().get(0).getAudiences().get(0).getAudienceURI();
   }

   private void checkValidityTime(Assertion assertion) {
      Date expirationDate = assertion.getConditions().getNotOnOrAfter().toDate();

      long diffInMillies = Math.abs(new Date().getTime() - expirationDate.getTime());
      long diff = TimeUnit.MINUTES.convert(diffInMillies, TimeUnit.MILLISECONDS);

      if (diff > renewalConfig.getRenewalPeriodMinutes()) {
         throw new ExpiredAssertionException("renewal is not allowed two hours beyond the assertion expiration");
      }
   }
}
