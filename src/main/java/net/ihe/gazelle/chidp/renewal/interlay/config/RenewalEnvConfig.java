package net.ihe.gazelle.chidp.renewal.interlay.config;

import net.ihe.gazelle.chidp.renewal.interlay.ws.RenewalConfig;

public class RenewalEnvConfig implements RenewalConfig {

   public static final String GZL_CH_RENEWAL_IDP_URL = "GZL_CH_RENEWAL_IDP_URL";
   public static final String GZL_CH_RENEWAL_TEST_PASSWORD = "GZL_CH_RENEWAL_TEST_PASSWORD";
   public static final String GZL_CH_RENEWAL_PERIOD_MINUTES = "GZL_CH_RENEWAL_PERIOD_MINUTES";

   private static final String DEFAULT_IDP_URL = "https://ehealthsuisse.ihe-europe.net/idp/profile/SAML2/SOAP/ECP";
   private static final String DEFAULT_TEST_PASSWORD = "azerty";
   public static final String DEFAULT_RENEWAL_PERIOD_MINUTES = "120";

   @Override
   public String getIdpEcpUrl() {
      return getEnv(GZL_CH_RENEWAL_IDP_URL, DEFAULT_IDP_URL);
   }

   @Override
   public String getUserTestPassword() {
      return getEnv(GZL_CH_RENEWAL_TEST_PASSWORD, DEFAULT_TEST_PASSWORD);
   }

   @Override
   public int getRenewalPeriodMinutes() {
      return Integer.parseInt(getEnv(GZL_CH_RENEWAL_PERIOD_MINUTES, DEFAULT_RENEWAL_PERIOD_MINUTES));
   }

   private String getEnv(String name, String defaultValue) {
      return System.getenv(name) != null ?
            System.getenv(name) :
            defaultValue;
   }

}
