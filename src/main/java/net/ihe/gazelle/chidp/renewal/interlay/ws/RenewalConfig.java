package net.ihe.gazelle.chidp.renewal.interlay.ws;

public interface RenewalConfig {
   /**
    * Get the CH IDP ECP endpoint to perform an AuthnRequest
    *
    * @return the ECP endpoint
    */
   String getIdpEcpUrl();

   /**
    * Get the password to authentify as a test user.
    *
    * @return the password
    */
   String getUserTestPassword();

   /**
    * Get the period (in minutes) after the expiration of the Assertion, in which it is still allowed to perform a
    * renewal.
    *
    * @return the renewal time-windows in minutes.
    */
   int getRenewalPeriodMinutes();
}
