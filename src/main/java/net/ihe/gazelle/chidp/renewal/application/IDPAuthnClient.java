package net.ihe.gazelle.chidp.renewal.application;

import org.opensaml.saml2.core.Assertion;

/**
 * HTTP back-end Client to trigger an SAML 2 Protocol authentication.
 */
public interface IDPAuthnClient {

   /**
    * Trigger an authentication to an SAML2 Idp, using ECP binding.
    *
    * @param issuer   Issuer of the authentication request.
    * @param username username of the user to authenticate.
    * @param password password of the user to authenticate.
    *
    * @return An SAMl2 assertion
    *
    * @throws AuthenticationException in case the IDP rejected the request.
    * @throws AuthnClientException    in case unexpected error happened, such as IO writing error, parsing exception...
    */
   Assertion authenticate(String issuer, String username, String password);

}
