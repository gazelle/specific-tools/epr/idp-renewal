package net.ihe.gazelle.chidp.renewal.interlay.ws;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class XmlUtcTimestampFormater {

   private XmlUtcTimestampFormater() {
   }

   public static String format(Date date) {
      DateFormat xmlDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'");
      xmlDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
      return xmlDateFormat.format(date);
   }

}
