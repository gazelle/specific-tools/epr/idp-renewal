package net.ihe.gazelle.chidp.renewal.interlay.factory;

import net.ihe.gazelle.chidp.renewal.application.IDPAuthnClient;
import net.ihe.gazelle.chidp.renewal.application.RenewalService;
import net.ihe.gazelle.chidp.renewal.application.RenewalServiceImpl;
import net.ihe.gazelle.chidp.renewal.interlay.ws.RenewalConfig;
import net.ihe.gazelle.chidp.renewal.interlay.ws.ShibbolethAuthnClient;

import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.inject.Named;

@Named("renewalFactory")
public class RenewalFactory {

   @Inject
   RenewalConfig renewalConfig;

   @Produces
   public RenewalService getRenewalService() {
      return new RenewalServiceImpl(getIDPAuthnClient(), renewalConfig);
   }

   @Produces
   public IDPAuthnClient getIDPAuthnClient() {
      return new ShibbolethAuthnClient(renewalConfig);
   }

}
