package net.ihe.gazelle.chidp.renewal.application;

public class AuthenticationException extends RuntimeException {
   private static final long serialVersionUID = 8892150338476918343L;

   public AuthenticationException() {
   }

   public AuthenticationException(String message) {
      super(message);
   }

   public AuthenticationException(String message, Throwable cause) {
      super(message, cause);
   }

   public AuthenticationException(Throwable cause) {
      super(cause);
   }
}
