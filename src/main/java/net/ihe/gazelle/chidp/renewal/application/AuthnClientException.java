package net.ihe.gazelle.chidp.renewal.application;

public class AuthnClientException extends RuntimeException{

    private static final long serialVersionUID = 6253285566166936187L;

    public AuthnClientException() {
    }

    public AuthnClientException(String message) {
        super(message);
    }

    public AuthnClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public AuthnClientException(Throwable cause) {
        super(cause);
    }
}
