package net.ihe.gazelle.chidp.renewal.application;

public class ExpiredAssertionException extends RuntimeException {
    private static final long serialVersionUID = 2260781726477564973L;

    public ExpiredAssertionException() {
    }

    public ExpiredAssertionException(String s) {
        super(s);
    }

    public ExpiredAssertionException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public ExpiredAssertionException(Throwable throwable) {
        super(throwable);
    }
}
