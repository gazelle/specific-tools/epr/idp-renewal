package net.ihe.gazelle.chidp.renewal.interlay.ws;

import net.ihe.gazelle.chidp.renewal.application.AuthenticationException;
import net.ihe.gazelle.chidp.renewal.application.AuthnClientException;
import net.ihe.gazelle.chidp.renewal.application.IDPAuthnClient;
import net.ihe.gazelle.chidp.renewal.application.RenewalService;
import net.ihe.gazelle.chidp.renewal.interlay.config.RenewalEnvConfig;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.config.AuthSchemes;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.xml.security.utils.Base64;
import org.opensaml.saml2.core.Assertion;
import org.opensaml.xml.io.UnmarshallingException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.xml.sax.SAXException;

import javax.xml.XMLConstants;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import static net.ihe.gazelle.chidp.renewal.application.RenewalService.ISSUER_TAGNAME;
import static net.ihe.gazelle.chidp.renewal.application.RenewalService.SAML_2_0_ASSERTION_NS;

/**
 * Implementation of an IDPAuthnClient for the CH EPR Idp.
 */
public class ShibbolethAuthnClient implements IDPAuthnClient {

   private static final String SAML_2_0_PROTOCOL_NS = "urn:oasis:names:tc:SAML:2.0:protocol";
   private static final String AUTHN_REQUEST_TAGNAME = "AuthnRequest";
   public static final String AUTHN_REQUEST_ID_ATTRIBUTE_NAME = "ID";
   public static final String AUTHN_REQUEST_ISSUE_INSTANT_ATTRIBUTE_NAME = "IssueInstant";

   private final SAML2AssertionMarshaller assertionMarshaller;
   private final HttpClient httpClient;
   private final RenewalConfig renewalConfig;

   public ShibbolethAuthnClient() {
      this(new RenewalEnvConfig());
   }

   public ShibbolethAuthnClient(RenewalConfig renewalConfig) {
      assertionMarshaller = new SAML2AssertionMarshaller();
      httpClient = HttpClientBuilder.create().build();
      this.renewalConfig = renewalConfig;
   }

   @Override
   public Assertion authenticate(String issuer, String username, String password) {
      String authnRequestBody = buildAuthnRequest(issuer);
      HttpPost post = buildPostRequest(authnRequestBody, username, password);
      try {
         HttpResponse response = httpClient.execute(post);
         return parseResponse(response);
      } catch (IOException e) {
         throw new AuthnClientException("Unable to perform authentication.", e);
      }

   }

   private String buildAuthnRequest(String issuer) {
      try {
         Document doc = getAuthnRequestTemplate();
         injectRequestData(doc, issuer);
         return printRequest(doc);
      } catch (ParserConfigurationException | IOException | SAXException | TransformerException e) {
         throw new AuthnClientException("Unable to build AuthnRequest.", e);
      }
   }

   private Document getAuthnRequestTemplate() throws ParserConfigurationException, SAXException, IOException {
      try (InputStream requestTemplateIS = getClass().getResourceAsStream("/template/authn-request.xml")) {
         DocumentBuilder builder = getDocumentBuilderFactory().newDocumentBuilder();
         Document doc = builder.parse(requestTemplateIS);
         doc.getDocumentElement().normalize();
         return doc;
      }
   }

   private void injectRequestData(Document doc, String issuer) {
      Element authnRequest = (Element) doc.getElementsByTagNameNS(SAML_2_0_PROTOCOL_NS, AUTHN_REQUEST_TAGNAME).item(0);

      authnRequest.setAttribute(AUTHN_REQUEST_ID_ATTRIBUTE_NAME, UUID.randomUUID().toString());
      authnRequest.setAttribute(AUTHN_REQUEST_ISSUE_INSTANT_ATTRIBUTE_NAME, XmlUtcTimestampFormater.format(new Date()));

      authnRequest.getElementsByTagNameNS(SAML_2_0_ASSERTION_NS, ISSUER_TAGNAME).item(0)
            .setTextContent(issuer);
   }


   private String printRequest(Node node) throws TransformerException {
      TransformerFactory tf = TransformerFactory.newInstance();
      tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_DTD, "");
      tf.setAttribute(XMLConstants.ACCESS_EXTERNAL_STYLESHEET, "");
      Transformer transformer = tf.newTransformer();
      StringWriter writer = new StringWriter();
      transformer.transform(new DOMSource(node), new StreamResult(writer));
      return writer.getBuffer().toString();
   }

   private HttpPost buildPostRequest(String requestBody, String username, String password) {
      try {
         HttpPost post = new HttpPost(renewalConfig.getIdpEcpUrl());
         post.setEntity(new StringEntity(requestBody));
         String encoding = Base64.encode((username + ":" + password).getBytes());
         post.addHeader("Authorization", AuthSchemes.BASIC + " " + encoding);
         post.addHeader("content-type", "text/xml");
         return post;
      } catch (UnsupportedEncodingException e) {
         throw new AuthnClientException("Unable to write authn request in HTTP Post body", e);
      }
   }


   private Assertion parseResponse(HttpResponse response) {
      if (response.getStatusLine().getStatusCode() != 200) {
         throw new AuthenticationException(parseKOResponse(response));
      }
      if (response.getEntity() == null) {
         throw new AuthnClientException("Unexpected empty body in authn response.");
      }

      try {
         return assertionMarshaller.unmarshall(
               getAssertionElement(parseAsXmlDocument(response.getEntity()))
         );
      } catch (UnmarshallingException e) {
         throw new AuthnClientException("Unable to parse Assertion from Authn Response.");
      }
   }

   private Document parseAsXmlDocument(HttpEntity body) {
      try (InputStream bodyIs = body.getContent()) {
         DocumentBuilder builder = getDocumentBuilderFactory().newDocumentBuilder();
         Document doc = builder.parse(body.getContent());
         doc.getDocumentElement().normalize();
         return doc;
      } catch (IOException | ParserConfigurationException | SAXException e) {
         throw new AuthnClientException("Unable to parse Authn response.", e);
      }
   }

   private Element getAssertionElement(Document doc) {
      return (Element) doc.getElementsByTagNameNS(SAML_2_0_ASSERTION_NS,
            RenewalService.ASSERTION_TAGNAME).item(0);
   }

   private String parseKOResponse(HttpResponse response) {
      String pattern = "Failure to authenticate with the IDP.\nHTTP Status: {0} {1}";
      List<Object> args = Arrays.asList(
            response.getStatusLine().getStatusCode(),
            response.getStatusLine().getReasonPhrase()
      );
      if (response.getEntity() != null) {
         try (InputStream is = response.getEntity().getContent()) {
            pattern += "\nHTTP Body: {2}";
            args.add(new String(is.readAllBytes(), StandardCharsets.UTF_8));
         } catch (IOException e) {
            throw new AuthnClientException("Unable to parse KO response.", e);
         }
      }
      return MessageFormat.format(pattern, args);
   }


   private DocumentBuilderFactory getDocumentBuilderFactory() throws ParserConfigurationException {
      DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
      factory.setFeature("http://apache.org/xml/features/disallow-doctype-decl", true);
      factory.setNamespaceAware(true);
      return factory;
   }
}
