FROM rg.fr-par.scw.cloud/tools/wildfly:26.1.1.Final

COPY target/ch-idp-renewal.war /opt/jboss/wildfly/standalone/deployments/

# Uncomment to enable debug
# EXPOSE 8787
# CMD ["/opt/jboss/wildfly/bin/standalone.sh", "-b", "0.0.0.0", "--debug", "*:8787"]